/***
* Fight Game
* {Author} Daniel Dimitrov
* {Date} 20.02.2017
*/

(function fightGameModule() {

	function Game() {
		
		console.log('game init');

		this.winner = '';
		this.deadHero = '';
		
		this.mainPowers = {
			health: 100,
			armor: {
				defence: 10,
				reduceDamage: 2
			},
			damage: 1,
			brutality: 10,
			brutalityTurn: 5
		};

		this.colors = {
			winner: 'background: #336699; color: #fff; padding: 3px 5px;',
			dead: 'background: #b00000; color: #fff; padding: 3px 5px;',
			heroOne: 'background: #0ba5da; color: #000; padding: 3px 5px;',
			heroTwo: 'background: #0dcb6f; color: #000; padding: 3px 5px;',
			brutality: 'background: #960c0c; color: #ccc; padding: 3px 5px; font-weight: bold;',
			armor: 'background: #d2d10d; color: #000; padding: 3px 5px; font-weight: bold;',
			round: 'background: #c00ac7; color: #fff; padding: 3px 25px; font-weight: bold; font-size: 16px;',
			fight: 'color: #b00000; padding: 3px 25px; font-weight: bold; font-size: 26px;'
		};

		this.heroOne = {};
		this.heroTwo = {};
		this.heroes = [];

		this.getHeroOneData();
		this.getHeroTwoData();
		
		this.changeHitPerson();

		console.log(this.heroOne, this.heroTwo);
		
		this.fight();
	};

	/***
	* Choose who is going to punch first
	*/
	Game.prototype.changeHitPerson = function() {

		var shuffle = this.shuffleHero(this.heroes);

		this.heroOne = this.heroes[parseInt(shuffle[0])];
		this.heroTwo = this.heroes[parseInt(shuffle[1])];
	};

	/***
	* Shuffle array with heroes
	* @param {Array} heroArr
	*/
	Game.prototype.shuffleHero = function(heroArr) {

		var arrKeys = Object.keys(heroArr);

		return arrKeys.sort(function() {
			return 0.5 - Math.random();
		});
	};

	/***
	* Set first hero object 
	*/
	Game.prototype.getHeroOneData = function() {
		
		console.log('heroOne');

		this.mainPowers = {
			name: 'Superman',
			health: 100,
			armor: {
				defence: 100,
				reduceDamage: 3
			},
			damage: 20,
			brutality: 50,
			brutalityTurn: 8
		};

		this.heroes.push(this.mainPowers);
	};

	/***
	* Set second hero object 
	*/
	Game.prototype.getHeroTwoData = function() {
		
		console.log('heroTwo');

		this.mainPowers = {
			name: 'Batman',
			health: 100,
			armor: {
				defence: 150,
				reduceDamage: 2
			},
			damage: 20,
			brutality: 20,
			brutalityTurn: 3
		};

		this.heroes.push(this.mainPowers);
	};

	/***
	* Call hero brutality
	* @param {Object} attackerHero
	* @param {Object} attackedHero
	*/
	Game.prototype.callBrutality = function(attackerHero, attackedHero) {
		
		console.log('%cBrutality - ' + attackerHero.name, this.colors.brutality);

		return attackedHero.health -= attackerHero.brutality;
	};

	/***
	* Call hero defence
	* @param {Object} attackerHero
	* @param {Object} defenceHero
	*/
	Game.prototype.armorProtection = function(attakerHero, defenceHero) {
		
		console.log('%carmorProtection - ' + defenceHero.name + ' - ' + defenceHero.armor.defence, this.colors.armor);

		var defence = (defenceHero.armor.defence / defenceHero.armor.reduceDamage).toFixed(), // 50
			damage; 

			if (defence > attakerHero.damage) { // 50 > 10
				damage = 0; 
				defenceHero.armor.defence -= (defence - (defence - attakerHero.damage)); // 150 = 50 - (50 - 10)
			} else {
				damage = attakerHero.damage - defence;
			}

		return damage;
	};

	/***
	* Check if one of the heroes is dead
	*/
	Game.prototype.isDead = function() {
		
		// console.log('isDead');

		if (this.heroOne.health <= 0) {

			this.deadHero = this.heroOne.name;
			this.winner = this.heroTwo.name;

		} else if (this.heroTwo.health <= 0) {

			this.deadHero = this.heroTwo.name;
			this.winner = this.heroOne.name

		} else {
			return false;
		}

		return true;
	};

	/***
	* Heroes fight 
	*/
	Game.prototype.fight = function() {
		
		console.log('%cFIGHT', this.colors.fight);
		
		var counter = 1;

		while(this.heroOne.health > 0 || this.heroTwo > 0) {
			
			console.log('%cround - ' + counter, this.colors.round);

			// check for brutalityTurn and increase the damage
			if (counter > 1 && (counter % this.heroOne.brutalityTurn === 0) === true) {
				this.callBrutality(this.heroOne, this.heroTwo);
			}

			// check for hero armor and decrease the damage
			if(counter > 1 && (counter % this.heroTwo.brutalityTurn === 0) === true) {
				this.callBrutality(this.heroTwo, this.heroOne);
			}

			// calculate the damege and reduce hero health
			this.calcDmg();			
			
			console.log('%c' + this.heroOne.name + ' health - ' + this.heroOne.health, this.colors.heroOne);
			console.log('%c' + this.heroTwo.name + ' health - ' + this.heroTwo.health, this.colors.heroTwo);
			console.log('==================================');
			
			// check if one of the hero is dead
			if (this.isDead()) {
				console.log('%c==========================================================', this.colors.armor);
				console.log('%cthe winner is - ' + this.winner, this.colors.winner);
				console.log('%cthe dead is - ' + this.deadHero, this.colors.dead);
				console.log('%c==========================================================', this.colors.armor);
				break;
			}
			
			this.changeHitPerson();

			counter++;
		}
	};

	/***
	* Calculate the damage of the heroes and reduce heros health 
	*/
	Game.prototype.calcDmg = function() {

		this.heroOne.health -= (this.heroOne.armor.defence > 0) ? this.armorProtection(this.heroTwo, this.heroOne) : this.heroTwo.damage;

		this.heroTwo.health -= (this.heroTwo.armor.defence > 0) ? this.armorProtection(this.heroOne, this.heroTwo) : this.heroOne.damage;
	};

	new Game();

})();